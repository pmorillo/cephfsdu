# Cephfsdu

CephFS directories xattr viewer.

## Usage

By default directories are sorted by usage.

```sh
$ cd /srv/longdd
$ sudo cephfsdu
DIR       USAGE      QUOTA      FILES  SUBDIRS  RFILES  RSUBDIRS
xxxxxxxx  59.14 GB   70.00 GB   3      10       146136  16834
xxxxxxxx  50.04 GB   50.00 GB   0      1        15858   417
xxxxxxxx  49.66 GB   80.00 GB   6      8        1038    447
xxxxxxxx  48.19 GB   60.00 GB   696    0        696     1
xxxxxxxx  24.77 GB   50.00 GB   1      3        43748   5356
xxxxxxxx  23.03 GB   50.00 GB   0      3        68698   420
xxxxxxxx  22.79 GB   50.00 GB   0      2        118998  17168
xxxxxxxx  19.19 GB   50.00 GB   0      2        129748  31762
# ...

Total : 404.82 GB
```

```sh
$ cephfsdu /srv/longdd/$USER
DIR               USAGE      QUOTA  FILES  SUBDIRS  RFILES  RSUBDIRS
vm_images         6.54 GB    0 B    1      0        1       1
venv              1.73 GB    0 B    0      2        10824   1228
keras             1.47 GB    0 B    6      1        23450   4
netboot           992.30 MB  0 B    0      3        16258   2228
nemo              626.01 MB  0 B    0      1        6804    990
nix               470.63 MB  0 B    0      2        43963   20124
visqol            164.53 MB  0 B    10     7        197     46
dkms-ice          132.13 MB  0 B    6      3        870     42
io500             34.43 MB   0 B    16     10       906     231
ice-udeb          6.41 MB    0 B    1      1        145     8
singularity       989.00 B   0 B    2      0        2       1
.virtfs_metadata  88.00 B    0 B    2      0        2       1

Total : 12.11 GB
```

Sort by number of recursive files.

```sh
$ cd /srv/tempdd
$ sudo cephfsdu -sort=rfiles
DIR          USAGE      QUOTA    FILES  SUBDIRS  RFILES    RSUBDIRS
xxxxxxxx     2.55 TB    0 B      113    4        15349974  8190
xxxxxxxx     195.52 GB  0 B      0      7        15208640  65747
xxxxxxxx     117.47 GB  0 B      0      1        9257574   68163
xxxxxxxx     587.13 GB  0 B      2      6        7707434   843
xxxxxxxx     3.05 TB    0 B      193    9        6876363   231228
xxxxxxxx     64.08 GB   0 B      0      8        508801    50526
xxxxxxxx     224.02 GB  0 B      0      3        281620    92548
xxxxxxxx     223.28 GB  0 B      0      9        273325    24256
# ...

Total : 13.30 TB
```

