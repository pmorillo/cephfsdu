package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"syscall"
	"text/tabwriter"

	"golang.org/x/sys/unix"
)

var (
	version     = "dev"
	versionFlag = flag.Bool("version", false, "Show version.")
	sortFlag    = flag.String("sort", "usage", "Sort dirs by usage|rfiles|rsubdirs.")
)

// Directory struct
type Directory struct {
	Path      string
	Name      string
	UID       uint32
	GID       uint32
	Mode      os.FileMode
	CephQuota int64
	CephUsage int64
	RFiles    int64
	RSubdirs  int64
	Files     int64
	Subdirs   int64
}

func main() {
	flag.Parse()
	if *versionFlag {
		fmt.Println(version)
		os.Exit(0)
	}
	path, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	if len(flag.Args()) >= 1 {
		path = flag.Args()[0]
	}
	dirs, err := getDirectories(path)
	if err != nil {
		fmt.Println(err)
	}

	sort.Slice(dirs, func(i, j int) bool {
		switch *sortFlag {
		case "usage":
			return dirs[i].CephUsage > dirs[j].CephUsage
		case "rfiles":
			return dirs[i].RFiles > dirs[j].RFiles
		case "rsubdirs":
			return dirs[i].RSubdirs > dirs[j].RSubdirs
		default:
			log.Fatalf("Invalid sort arguments %s", *sortFlag)
		}
		return false
	})

	var total int64 = 0
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
	fmt.Fprintln(w, "DIR\tUSAGE\tQUOTA\tFILES\tSUBDIRS\tRFILES\tRSUBDIRS\t")
	for _, d := range dirs {
		line := fmt.Sprintf("%s\t%s\t%s\t%d\t%d\t%d\t%d\t", d.Name, readableQuantity(float64(d.CephUsage)), readableQuantity(float64(d.CephQuota)), d.Files, d.Subdirs, d.RFiles, d.RSubdirs)
		fmt.Fprintln(w, line)
		total += d.CephUsage
	}
	w.Flush()
	fmt.Printf("\nTotal : %s\n", readableQuantity(float64(total)))
}

func getDirectories(path string) (dirs []Directory, err error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		if file.IsDir() {
			d := Directory{}
			d.Name = file.Name()
			d.UID = file.Sys().(*syscall.Stat_t).Uid
			d.GID = file.Sys().(*syscall.Stat_t).Gid
			dirPath := fmt.Sprintf("%s/%s", path, d.Name)
			d.CephQuota, err = getQuota(dirPath)
			if err != nil {
				d.CephQuota = 0
			}
			d.CephUsage, err = getUsage(dirPath)
			if err != nil {
				return dirs, err
			}
			d.RFiles, err = getRFiles(dirPath)
			if err != nil {
				return dirs, err
			}
			d.RSubdirs, err = getRSubdirs(dirPath)
			if err != nil {
				return dirs, err
			}
			d.Files, err = getFiles(dirPath)
			if err != nil {
				return dirs, err
			}
			d.Subdirs, err = getSubdirs(dirPath)
			if err != nil {
				return dirs, err
			}
			dirs = append(dirs, d)
		}
	}
	return dirs, nil
}

func readableQuantity(size float64) string {
	if size == 0 {
		return "0 B"
	} else {
		units := []string{"B", "KB", "MB", "GB", "TB", "EB"}
		index := 0
		for size >= 1024 && index < (len(units)-1) {
			size = size / 1024
			index++
		}
		return fmt.Sprintf("%.02f %s", size, units[index])
	}
}

func getCephxattr(dir string, attr string) (int64, error) {
	dest := make([]byte, 128)
	value, err := unix.Getxattr(dir, attr, dest)
	if err != nil {
		return 0, err
	}
	dest = make([]byte, value+1)
	_, err = unix.Getxattr(dir, attr, dest)
	if err != nil {
		return 0, err
	}
	resultStr := string(dest[:value])
	result, _ := strconv.ParseInt(resultStr, 10, 64)
	return result, nil
}

func getUsage(dir string) (int64, error) {
	usage, err := getCephxattr(dir, "ceph.dir.rbytes")
	if err != nil {
		return 0, err
	}
	return usage, nil
}

func getQuota(dir string) (int64, error) {
	quota, err := getCephxattr(dir, "ceph.quota.max_bytes")
	if err != nil {
		return 0, err
	}
	return quota, nil
}

func getRFiles(dir string) (int64, error) {
	rfiles, err := getCephxattr(dir, "ceph.dir.rfiles")
	if err != nil {
		return 0, err
	}
	return rfiles, nil
}

func getRSubdirs(dir string) (int64, error) {
	rsubdirs, err := getCephxattr(dir, "ceph.dir.rsubdirs")
	if err != nil {
		return 0, err
	}
	return rsubdirs, nil
}

func getFiles(dir string) (int64, error) {
	files, err := getCephxattr(dir, "ceph.dir.files")
	if err != nil {
		return 0, err
	}
	return files, nil
}

func getSubdirs(dir string) (int64, error) {
	subdirs, err := getCephxattr(dir, "ceph.dir.subdirs")
	if err != nil {
		return 0, err
	}
	return subdirs, nil
}
